<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * Format JSON
     *
     * @param $status
     * @param $data
     * @return array
     */
    public function json_result($status, $data)
    {
        return compact('status','data');
    }

    /**
     * success response method.
     *
     * @param $result
     * @param null $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result, $message = null)
    {
        $response = [
            'status' => 'Ok',
            'result'    => $result,
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @param $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => 'Error',
            'errors' => $error,
        ];


        if(!empty($errorMessages)){
            $response['errors'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

}
