<?php

namespace App\Http\Controllers;

use App\Repository\CompanyRepository;
use Illuminate\Http\Request;

class CargoController extends Controller
{
    public $company;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->company = $companyRepository;
    }

    public function index()
    {
        $companies = $this->company->paginate(15);

        return view('cargo.index', compact('companies'));
    }

    public function show($slug)
    {
        $company = $this->company->findBySlug($slug);
        $companies = $this->company->paginate(15);

        return view('cargo.show', compact('company','companies'));
    }
}
