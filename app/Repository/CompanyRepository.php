<?php


namespace App\Repository;
use App\Company;

class CompanyRepository
{
    /**
     * @param int $page
     * @return mixed
     */
    public function paginate($page = 1)
    {
        $limit = 15;

        return Company::with(['shipments' => function($q) {
            $q->with('cargos');
        }])->offset(( $limit * $page) - $limit)->limit($limit)->orderBy('name')->paginate();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return Company::where('slug',$slug)->first();
    }
}
