@extends('layouts.basic')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-4">

                @include('cargo.partials.list')

            </div>
            <div class="col-12 col-lg-8">
                @include('cargo.partials.company_detail')
            </div>
        </div>
    </div>
@endsection
