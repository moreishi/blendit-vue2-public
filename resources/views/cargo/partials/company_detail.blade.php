<div class="card">
    <div class="card-body">
        <h5 class="card-title">{{ $company->name }}</h5>
        <h6 class="card-subtitle mb-4 text-muted"><a href="mailto:{{ $company->email }}">{{ $company->email }}</a></h6>
        <p class="card-text">Number of required cargo bays</p>
        <div class="row">
            <div class="col-12 col-lg-6">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Cargo Boxes</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
