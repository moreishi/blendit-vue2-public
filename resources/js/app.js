/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

import 'es6-promise/auto';
import combined_stores from './store/index';
import Vuex from 'vuex'
import VueRouter from 'vue-router';
import VueSweetalert2 from 'vue-sweetalert2';
import { Form, HasError, AlertError } from 'vform'
import AppComponent from './components/AppComponent';
import { routes } from "./routes";


window.Vue = require('vue');
window.Form = Form;

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueSweetalert2);

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Company
Vue.component('CompanyListComponent', require('./components/company/CompanyListComponent.vue').default);
Vue.component('CompanyDetailsComponent', require('./components/company/CompanyDetailsComponent.vue').default);

// System
Vue.component('ProgressBarComponent', require('./components/common/ProgressBarComponent.vue').default);
Vue.component('LoadDataComponent', require('./components/common/LoadDataComponent.vue').default);
Vue.component('NavigationComponent', require('./components/common/NavigationComponent.vue').default);
Vue.component('App', require('./components/AppComponent.vue').default);
Vue.component('MainContentComponent', require('./components/common/MainContentComponent.vue').default);
Vue.component('NoDataComponent', require('./components/common/NoDataComponent.vue').default);

// Store
const store = new Vuex.Store(combined_stores);

// Router
const router = new VueRouter({
    mode: 'history',
    routes: routes,
});



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: { AppComponent },
    router,
    store
});
