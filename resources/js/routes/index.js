import CompanyDetailsComponent from '../components/company/CompanyDetailsComponent';

export const routes = [
    {
        path: '/',
        name: 'home',
        component: CompanyDetailsComponent
    },
    {
        path: '/:id',
        name: 'companies',
        component: CompanyDetailsComponent,
    },
];
