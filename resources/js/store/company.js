export default {
    state: {
        companies: [],
        selected_slug: '',
        selected_cargos: '',
        paginate: {
            next_url: '',
            prev_url: '',
            current_url: '',
            total: 0,
            isMaxed: false
        },
        shipment: {
            cargos: '',
            bays: 0
        }
    },
    mutations: {
        set_companies_next_url: function(state, payload) {
            localStorage.setItem('companies_next_url', payload);
            return state.paginate.next_url = payload;
        },
        set_companies: function(state, payload) {
            let _state = state.companies = [...state.companies, ...payload];
            localStorage.setItem('companies', JSON.stringify(_state));
            return _state;
        },
        set_companies_selected_slug: function(state, payload) {
            return state.selected_slug = payload;
        },
        set_companies_selected_cargos: function(state, payload) {
            return state.selected_cargos = payload;
        },
        set_companies_paginate_maxed: function(state, payload) {
            localStorage.setItem('companies_is_maxed', payload);
            return state.paginate.isMaxed = payload;
        },
        set_companies_cargo_units: function(state, payload) {
            return state.shipment.cargos = payload;
        },
        set_companies_cargo_bays: function(state, payload) {
            return state.shipment.bays = payload;
        },
        set_companies_cargo_updates: function(state, payload) {
            let companies = state.companies;
            let companyIndex = companies.findIndex(company => company.id === payload.id);
            companies[companyIndex] = payload;
            state.companies = companies;
            localStorage.setItem('companies', JSON.stringify(state.companies));
        }

    },
    actions: {},
    getters: {
        get_companies: function(state) {
            return state.companies;
        },
        get_companies_next_url: function(state) {
            return state.paginate.next_url;
        },
        get_companies_selected_slug: function(state) {
            return state.companies.find(company => company.slug === state.selected_slug)
        },
        get_companies_selected_cargos: function(state) {
            return state.selected_cargos;
        },
        get_companies_paginate_maxed: function(state) {
            return state.paginate.isMaxed
        },
        get_companies_cargo_units: function(state) {
            return state.shipment.cargos;
        },
        get_companies_cargo_bays: function(state) {
            return state.shipment.bays;
        }
    }
}
