import company from './company';
import system from './system';

const store = {
    modules: {
        system: system,
        company: company
    }
}

export default store;
