export default {
    state: {
        inProgress: false
    },
    mutations: {
        set_system_progress: function(state, payload) {
            return state.inProgress = payload;
        }
    },
    actions: {},
    getters: {
        get_system_progress: function(state) {
            return state.inProgress;
        }
    }
}
