# SpaceX Cargo Planner

A simple app for SpaceX Cargo. A test solution using laravel + VueJs

## Requirements
- PHP v5 to latest (recommended PHP 7.3)
- Composer package manager
- NPM and NodeJS latest
- GIT
- Any document editor. # e.g notepadt, vi, nano

## Installation

Clone the repo to your local machine

```bash
git clone git@gitlab.com:moreishi/blendit-vue2-public.git
```

## Usage

Change directory inside cloned repository

```bash
cd blendit-vue2-public
```

Within the directory of the app run `composer install`. To install the laravel dependencies.

```bash
composer install
```

Within the directory of the app run `npm install`. To install the dependencies for vue.

```bash
npm install
```

Run the command below to compile the vue files and its dependencies.

```bash
npm run dev
```

Setup the `.env` file

```bash
cp .env.example .env
```

Get the correct path of the directory by running the command below.

```bash
pwd # save the directory path result you will need this in the later staps.
```

Open `.env` using your favorite editor. For this sample we use `nano`.

```bash
sudo nano .env
```

Change the following line

```bash
DB_CONNECTION=sqlite
DB_DATABASE= # place your pwd result here then add /database/database.sqlite

#sample
DB_DATABASE=/Users/user1/Desktop/blendit-vue2-public/database/database.sqlite
```

Create `database.sqlite` file within `./database` directory.

```bash
touch ./database/database.sqlite # check the database folder if database.sqlie is created
```

We need to run the migration for sample data. Run the command below within the app directory.

```bash
php artisan migrate:fresh --seed
```

Generate unique app key.

```bash
php artisan key:generate
```

Run the local server

```
php artisan serve --port=8000
```


## Note
This is a MVC type of application, Laravel + VueJs. Some VueJs files are used within the php blade files. Meaning both blade and vuejs files are dependent to one another.

## Vue files location
All vue javascript files are located to this path.
- resources/js

## Blade files location
All vue javascript files are located to this path.
- resources/views

