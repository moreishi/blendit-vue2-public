<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Shipment;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Shipment::class, function (Faker $faker) {

    return [
        'unique_reference_id' => md5(Str::random(16))
    ];

});
