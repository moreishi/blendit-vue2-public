<?php

use Illuminate\Database\Seeder;
use App\Shipment;
use App\Company;

class ShipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::all();

        foreach ($companies as $company) {
            factory(Shipment::class)->create([
                'company_id' => $company->id
            ])->pluck('id');
        }
    }
}
