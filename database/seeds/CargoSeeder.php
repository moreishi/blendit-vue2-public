<?php

use Illuminate\Database\Seeder;
use App\Shipment;
use App\Cargo;

class CargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shipments = Shipment::all();

        foreach ($shipments as $shipment) {
            $qty = (int) rand(1,5);
            for($i = 0; $i < $qty; $i++) {
                $unit = (int) rand(1,10);
                factory(Cargo::class)->create([
                    'unit' => $unit,
                    'shipment_id' => $shipment->id]);
            }
        }
    }
}
